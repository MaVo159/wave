package wave

import "bitbucket.org/MaVo159/sort"
import "fmt"
import "os"
import "bufio"
import "io"
import "strings"
import "strconv"
import "math"

type Wave []float64

func ExportTxt(fn string, waves ...Wave) {
	for _, w := range waves {
		if waves[0].Len() != w.Len() {
			panic("Wave: ExportTxt: Waves not of same length")
		}
	}
	f, err := os.Create(fn)
	defer f.Close()
	if err != nil {
		panic("Wave: ExportTxt: Error creating file: " + err.Error())
	}
	for i := 0; i < waves[0].Len(); i++ {
		for j := range waves {
			if j > 0 {
				fmt.Fprint(f, "\t")
			}
			fmt.Fprint(f, waves[j][i])
		}
		if i < waves[0].Len()-1 {
			fmt.Fprintln(f)
		}
	}
}

//Imports waves from textfile. Each passed wave is used to store one column.
//Additional columns are ignored. Pass nil to skip a column. The number of rows and
//columns is returned. If a passed wave does not have enough capacity, a new one is
//allocated.
func ImportTxt(fn string, wave ...*Wave) (rows, cols int) {
	f, err := os.Open(fn)
	defer f.Close()
	bf := bufio.NewReader(f)
	if err != nil {
		panic("Wave: ImportTxt: Error opening file: " + err.Error())
	}
	for {
		line, err := bf.ReadString('\n')
		line = strings.TrimSuffix(line, "\n")
		if err != nil && err != io.EOF {
			panic("Wave: ImportTxt: Error reading line: " + err.Error())
		}
		split := strings.Split(line, "\t")
		if rows == 0 {
			cols = len(split)
		}
		if len(split) >= len(wave) {
			for i := 0; i < len(wave); i++ {
				parsed, perr := strconv.ParseFloat(split[i], 64)
				if perr != nil {
					panic("Wave: ImportTxt: Error parsing number: " + perr.Error())
				}
				if wave[i] != nil {
					*wave[i] = append(*wave[i], parsed)
				}
			}
			rows++
		} else if err == io.EOF {
			return
		} else {
			panic("Wave: ImportTxt: Not enough items in row")
		}
	}
}

func Transpose(waves ...Wave) []Wave {
	if len(waves) == 0 {
		return nil
	}
	for _, w := range waves {
		if waves[0].Len() != w.Len() {
			panic("Wave: ExportTxt: Waves not of same length")
		}
	}
	r := make([]Wave, waves[0].Len())
	for i := range r {
		r[i] = make(Wave, len(waves))
	}
	for i := range waves {
		for j := range waves[0] {
			r[j][i] = waves[i][j]
		}
	}
	return r
}

//Applies the function passed as argument to every element of the wave
func (x Wave) ApplyFunc(f func(int64, float64) float64) {
	for i := range x {
		x[i] = f(int64(i), x[i])
	}
}

func (x Wave) Sqrt() {
	for i := range x {
		x[i] = math.Sqrt(x[i])
	}
}

func (x Wave) Log() {
	for i := range x {
		x[i] = math.Log(x[i])
	}
}

func (x Wave) PowS(s float64) {
	for i := range x {
		x[i] = math.Pow(x[i], s)
	}
}

func (x Wave) Pow(w Wave) {
	for i := range x {
		x[i] = math.Pow(x[i], w[i])
	}
}

func (x Wave) Floor() {
	for i := range x {
		x[i] = math.Floor(x[i])
	}
}

func (x Wave) ContainsS(v float64) bool {
	for _, w := range x {
		if v == w {
			return true
		}
	}
	return false
}

func (x Wave) Ceil() {
	for i := range x {
		x[i] *= math.Ceil(x[i])
	}
}

func (x Wave) Copy() Wave {
	r := make(Wave, len(x))
	copy(r, x)
	return r
}

//Turns wave into cumulative wave
func (x Wave) Cum() {
	var sum float64
	for i := range x {
		sum += x[i]
		x[i] = sum
	}
}

func (x *Wave) AppendS(v float64) {
	l := len(*x)
	if l == cap(*x) {
		o := *x
		*x = make([]float64, l, (l/1024+1)*1024)
		copy(*x, o)
	}
	*x = append(*x, v)
}

//Insert value between position i-1 and i. The index of v after insertion is i.
func (x *Wave) InsertS(i uint64, v float64) {
	l := len(*x)
	o := *x
	x.Extend(1)
	copy((*x)[i+1:l+1], o[i:l])
	(*x)[i] = v
}

//Insert value between position i-1 and i+len(v)-1. The index of the first value of v after insertion is i.
func (x *Wave) Insert(i uint64, v Wave) {
	l := uint64(len(*x))
	lv := uint64(len(v))
	o := *x
	x.Extend(lv)
	copy((*x)[i+lv:l+lv], o[i:l])
	copy((*x)[i:i+lv], v)
}

//Extends x by i elements. The last i elements are set to 0.
func (x *Wave) Extend(i uint64) {
	l := uint64(len(*x))
	if l+i > uint64(cap(*x)) {
		o := *x
		*x = make([]float64, l+i, (l/1024+i/1024+1)*1024)
		copy(*x, o[0:l])
	} else {
		*x = (*x)[0 : l+i]
		for j := l; j < l+i; j++ {
			(*x)[j] = 0
		}
	}
}

//Resulting wave element i contains x[i]-x[i+1]
func (x Wave) Diff() Wave {
	r := Make(x.Len() - 1)
	o := x[0]
	for i, v := range x[1:len(x)] {
		r[i] = v - o
		o = v
	}
	return r
}

func Make(l int) Wave {
	return make(Wave, l)
}

func (x Wave) AddS(s float64) {
	for i := range x {
		x[i] += s
	}
}

func (x Wave) MulS(s float64) {
	for i := range x {
		x[i] *= s
	}
}

func (x Wave) DivS(s float64) {
	for i := range x {
		x[i] /= s
	}
}

func (x Wave) SubS(s float64) {
	for i := range x {
		x[i] -= s
	}
}

func (x Wave) SetS(s float64) {
	for i := range x {
		x[i] = s
	}
}

func (x Wave) Set(w Wave) {
	if x.Len() != w.Len() {
		panic("Wave: Set: w not of same length as x")
	}
	for i := range x {
		x[i] = w[i]
	}
}

func (x Wave) Add(w Wave) {
	if x.Len() != w.Len() {
		panic("Wave: Add: w not of same length as x")
	}
	for i := range x {
		x[i] += w[i]
	}
}

//Adds m*w to x. m is a scalar
func (x Wave) MulAddS(w Wave, m float64) {
	if x.Len() != w.Len() {
		panic("Wave: Add: w not of same length as x")
	}
	for i := range x {
		x[i] += w[i] * m
	}
}

//Adds m*w to x. m is a wave. m*w is computed elementwise.
func (x Wave) MulAdd(w, m Wave) {
	if x.Len() != w.Len() || x.Len() != m.Len() {
		panic("Wave: Add: m or w not of same length as x")
	}
	for i := range x {
		x[i] += w[i] * m[i]
	}
}

func (x Wave) Sub(w Wave) {
	if x.Len() != w.Len() {
		panic("Wave: Sub: w not of same length as x")
	}
	for i := range x {
		x[i] -= w[i]
	}
}

func (x Wave) Mul(w Wave) {
	if x.Len() != w.Len() {
		panic("Wave: Mul: w not of same length as x")
	}
	for i := range x {
		x[i] *= w[i]
	}
}

func (x Wave) Sum() float64 {
	var sum float64
	for _, e := range x {
		sum += e
	}
	return sum
}

func (x Wave) Div(w Wave) {
	if x.Len() != w.Len() {
		panic("Wave: Div: w not of same length as x")
	}
	for i := range x {
		x[i] /= w[i]
	}
}

func (x Wave) MinMax() (min, max float64) {
	min = math.Inf(1)
	max = math.Inf(-1)
	for _, v := range x {
		if v < min {
			min = v
		}
		if v > max {
			max = v
		}
	}
	return
}

//Returns a wave with counts, central bin value and width of each bin
func (x Wave) Histogram(nbins uint64) (counts, bins Wave) {
	min, max := x.MinMax()
	width := (max - min) / float64(nbins)
	counts, bins = Make(int(nbins)), Make(int(nbins))
	for i := range bins {
		bins[i] = min + width*(float64(i)+0.5)
	}
	for _, v := range x {
		for i := len(bins) - 1; i >= 0; i-- {
			if v >= min+width*float64(i) {
				counts[i]++
				break
			}
		}
	}
	return
}

//Returns a wave with counts, central bin value and width of each bin
func Histogram2D(x, y Wave, nxbins, nybins uint64) (counts, xbins, ybins Wave) {
	if x.Len() != y.Len() {
		panic("Wave: Add: x not of same length as y")
	}
	xmin, xmax := x.MinMax()
	ymin, ymax := y.MinMax()
	xwidth := (xmax - xmin) / float64(nxbins)
	ywidth := (ymax - ymin) / float64(nybins)
	xbins, ybins = Grid2d(xmin, xwidth, int(nxbins), ymin, ywidth, int(nybins))
	counts = Make(xbins.Len())
VLOOP:
	for iv, xv := range x {
		yv := y[iv]
		for by := nybins - 1; by >= 0; by-- {
			if yv >= ybins[by] {
				for bx := nxbins - 1; bx >= 0; bx-- {
					if xv >= xbins[bx*nybins] {
						counts[bx*nybins+by]++
						continue VLOOP
					}
				}
				panic("BUG: value not in xrange")
			}
		}
		panic("BUG: value not in yrange")
	}
	xbins.AddS(xwidth * 0.5)
	ybins.AddS(ywidth * 0.5)
	return
}

func Linspace(off, step float64, num uint64) (x Wave) {
	x = Make(int(num))
	for i := uint64(0); i < num; i++ {
		x[i] = off + float64(i)*step
	}
	return
}

func Grid2d(xoff, xstep float64, xnum int, yoff, ystep float64, ynum int) (x, y Wave) {
	x = Make(xnum * ynum)
	y = Make(xnum * ynum)
	for i := range x {
		xi := float64(i / ynum)
		yi := float64(i % ynum)
		x[i] = xoff + xstep*xi
		y[i] = yoff + ystep*yi
	}
	return
}

//Trim waves to specified range [l:h) of element indices
func Slice(l, h int, w ...*Wave) {
	if l < 0 {
		panic("Wave: Slice: l smaller than 0")
	}
	if h < l {
		panic("Wave: Slice: h smaller than l")
	}
	for _, e := range w {
		if e.Len() < h {
			panic("Wave: Slice: Length smaller than h")
		}
		*e = (*e)[l:h]
	}
}

//Sorts wave in ascending and performs same permutations on supplied y waves.
func (x Wave) Sort(y ...Wave) {
	for _, ye := range y {
		if x.Len() != ye.Len() {
			panic("Wave: Sort: y not of same length as x")
		}
	}
	swap := func(i, j int) {
		x[i], x[j] = x[j], x[i]
		for k := range y {
			y[k][i], y[k][j] = y[k][j], y[k][i]
		}
	}
	sort.Sort(func(i, j int) bool { return lessNan(x[i], x[j]) }, swap, len(x))
}

//Returns true if x is sorted in ascending order
func (x Wave) IsSorted() bool {
	return sort.IsSorted(func(i, j int) bool { return lessNan(x[i], x[j]) }, x.Len())
}

//Returns same wave if sorted or a sorted copy if unsorted
func (x Wave) Sorted() Wave {
	if !x.IsSorted() {
		x = x.Copy()
		x.Sort()
	}
	return x
}

//Calculates variance of wave.
func (x Wave) Mean() float64 {
	return x.Sum() / float64(x.Len())
}

//Calculates variance of wave.
func (x Wave) Var() float64 {
	mean := x.Mean()
	var squares float64
	for _, v := range x {
		diff := (v - mean)
		squares += diff * diff
	}
	return squares
}

//Calculates standard deviation "sqrt(Var/(Len-1))".
func (x Wave) Sd() float64 {
	return math.Sqrt(x.Var() / float64(x.Len()-1))
}

//Calculates mean variance and standard deviation of wave.
func (x Wave) Stat() (m, v, s float64) {
	m = x.Mean()
	for _, val := range x {
		diff := (val - m)
		v += diff * diff
	}
	s = math.Sqrt(v / float64(x.Len()-1))
	return
}

func isNan(x float64) bool {
	return !(x == x)
}

func lessNan(x, y float64) bool {
	return x < y || !isNan(x) && isNan(y)
}

func equalNan(x, y float64) bool {
	return x == y || isNan(x) && isNan(y)
}

//Returns true if x and y have the same values.
//This is not the same as the evalualtion of equal (NaN==NaN: false).
func Identical(x, y Wave) bool {
	if x.Len() != y.Len() {
		return false
	}
	for i := range x {
		if !equalNan(x[i], y[i]) {
			return false
		}
	}
	return true
}

//Adds Y values of arguments at identical X positions to receiver.
//y: Is set to sum of original value and other Y values with same X
//x: X values corresponding to Y values in y
//xy: pairs of waves with X and Y values
//x and y might be sorted in the process (by x)
func (y Wave) AddAt(x Wave, xy ...Wave) {
	if len(xy)%2 != 0 {
		panic("Wave: AddAt: Length of xy not multiple of 2")
	}
	for i := 0; i < len(xy); i += 2 {
		if xy[i].Len() != xy[i+1].Len() {
			panic("Wave: AddAt: Length of x and y in a pair not equal")
		}
		if !xy[i].IsSorted() {
			xy[i] = xy[i].Copy()
			xy[i+1] = xy[i+1].Copy()
			xy[i].Sort(xy[i+1])
		}
	}
	x.Sort(y)
	j := make([]int, len(xy)/2)
	var lastxe, yd float64
	for xei, xe := range x {
		if xei == 0 || xe != lastxe {
			lastxe = xe
			yd = 0
			for i := 0; i < len(xy)/2; i++ {
				for j[i] < xy[2*i].Len() {
					if lessNan(xy[2*i][j[i]], xe) {
						j[i]++
					} else if equalNan(xy[2*i][j[i]], xe) {
						yd += xy[2*i+1][j[i]]
						j[i]++
					} else {
						break
					}
				}
			}
		}
		y[xei] += yd
	}
}

//Returns union all elements of input waves in new sorted wave
func Unique(x ...Wave) Wave {
	//Sort
	for i := range x {
		x[i] = x[i].Sorted()
	}
	//Count unique elements
	j := make([]int, len(x))
	l := 0
	for {
		//Find smallest element
		var s int = -1
		for i := range x {
			if j[i] < len(x[i]) {
				if s == -1 || lessNan(x[i][j[i]], x[s][j[s]]) {
					s = i
				}
			}
		}
		if s == -1 {
			break
		}
		sv := x[s][j[s]]
		//Increase index
		for i := range x {
			for {
				if j[i] < len(x[i]) {
					if equalNan(x[i][j[i]], sv) {
						j[i]++
					} else {
						break
					}
				} else {
					break
				}
			}
		}
		//Increase count
		l++
	}
	r := make(Wave, 0, l)
	//Fill result vector
	j = make([]int, len(x))
	for {
		//Find smallest element
		var s int = -1
		for i := range x {
			if j[i] < len(x[i]) {
				if s == -1 || lessNan(x[i][j[i]], x[s][j[s]]) {
					s = i
				}
			}
		}
		if s == -1 {
			break
		}
		sv := x[s][j[s]]
		r = append(r, sv)
		//Increase index
		for i := range x {
			for {
				if j[i] < len(x[i]) {
					if equalNan(x[i][j[i]], sv) {
						j[i]++
					} else {
						break
					}
				} else {
					break
				}
			}
		}
	}
	return r
}

func (x Wave) Len() int { return len(x) }
