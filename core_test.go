package wave

import "testing"
import "math"

func TestUnique(t *testing.T) {
	a := Wave{0, 1, 2, 3}
	b := Wave{-1, 2, 2, 4, 8}
	c := Wave{0, 4, 8, 16, math.Inf(-1)}
	d := Wave{math.Inf(1), 1, 2, math.NaN()}
	q, r := Unique(a, b), Wave{-1, 0, 1, 2, 3, 4, 8}
	if !Identical(q, r) {
		t.Fatal(q, "!=", r)
	}
	q, r = Unique(c, d), Wave{math.Inf(-1), 0, 1, 2, 4, 8, 16, math.Inf(1), math.NaN()}
	if !Identical(q, r) {
		t.Fatal(q, "!=", r)
	}
	q, r = Unique(a, b, c, d), Wave{math.Inf(-1), -1, 0, 1, 2, 3, 4, 8, 16, math.Inf(1), math.NaN()}
	if !Identical(q, r) {
		t.Fatal(q, "!=", r)
	}
}

func TestAddAt(t *testing.T) {
	x := Wave{math.Inf(-1), 1, 2, 2, 4, math.NaN()}
	y := Wave{-1, -1, -1, 0, -1, -1}
	x1 := Wave{-1, 2, 2, 4, 8}
	x2 := Wave{0, 4, 16, math.Inf(-1)}
	y1 := Wave{1, 7, 10, -3, 8}
	y2 := Wave{0, 4, 16, -1}
	y.AddAt(x, x1, y1, x2, y2)

	r := Wave{-2, -1, 16, 17, 0, -1}
	if !Identical(y, r) {
		t.Fatal(y, "!=", r)
	}
}

func TestInsert(t *testing.T) {
	x := Wave{0, 1, 2, 3, 4, 5, 6}
	rS := Wave{0, 1, 2, 3, 10, 4, 5, 6}
	rW := Wave{0, 1, 2, 3, 10, 20, 30, 4, 5, 6}
	c := x.Copy()
	if c.InsertS(4, 10); !Identical(c, rS) {
		t.Fatal(c, "!=", rS)
	}
	c = x.Copy()
	if c.Insert(4, Wave{10, 20, 30}); !Identical(c, rW) {
		t.Fatal(c, "!=", rW)
	}

}

func TestAppend(t *testing.T) {
	x := Wave{0, 1, 2, 3, 4, 5, 6}
	rA := Wave{0, 1, 2, 3, 4, 5, 6, 7}
	if x.AppendS(7); !Identical(x, rA) {
		t.Fatal(x, "!=", rA)
	}
}

func TestExtend(t *testing.T) {
	x := Wave{0, 1, 2, 3, 4, 5, 6}
	r := Wave{0, 1, 2, 3, 4, 5, 6, 0, 0, 0}
	x.Extend(3)
	if !Identical(x, r) {
		t.Fatal(x, "!=", r)
	}
}

func TestDiff(t *testing.T) {
	x := Wave{0, 1, 2, 4, 4, 5, 6}
	r := Wave{1, 1, 2, 0, 1, 1}
	d := x.Diff()
	if !Identical(d, r) {
		t.Fatal(d, "!=", r)
	}
}
